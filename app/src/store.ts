import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loggedin: false,
    user: {}
  },
  mutations: {
    login(state) {
      state.loggedin = true;
    },
    logout(state) {
      state.loggedin = false;
    },
    addUser(state, user) {
      state.user = user;
    }
  },
  actions: {
    async login({ commit, state }, auth) {

      try {
        let url = process.env.VUE_APP_API_HOST + "/login";

        const data = new FormData();
        data.append("nickname", auth.email);
        data.append("password", auth.password);

        // Login and get token
        const login = await Vue.axios.post(process.env.VUE_APP_API_HOST + "/login", data);
        const token = login.data.data.token;

        // after sucessful login, get the user and save it to state
        const user = await Vue.axios.get(process.env.VUE_APP_API_HOST + "/user", { params: { "token": token } });
        commit("addUser", user.data.data);

        // Set user as loggedin
        commit("login");

        router.push(process.env.VUE_APP_DASHBOARD_PATH);

      } catch (error) {
        console.error(JSON.stringify(error));
      }
    },

    async logout({ commit }) {
      commit("logout");
    }
  }
})
